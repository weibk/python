### 列表函数&方法

#### 函数

##### 1 返回列表元素个数 len(list)

```python
list1 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
print(len(list1))
# 9
```

##### 2 返回列表元素最大值 max(list)

- 当列表中元素全部为字符串时，根据每个字符串中第一个字符的`ASCll` 或 `Unicode`对应的数值来比较，如果第一个字符的`ASCll` 或 `Unicode` 对应数值相同，则比较第二个字符，一次类推。

```python
list1 = ['maria', 'jack', '张三', '李四']
print(ord('m'))
# 109
print(ord('j'))
# 106
print(ord('张'))
# 24352
print(ord('李'))
# 26446
print(max(list1))
# 李四
```

- 当列表中元素全部为数值时，直接根据数值大小来比较。

```python
list1 = [1, 2, 6, 5, 8, 9, 10, 15]
print(max(list1))
# 15
```

- 当列表中元素不全是字符串或不全是数值时，无法比较出最大值。

```python
list1 = ['maria', 'jack', '张三', '李四', 2, 5]
print(max(list1))
# TypeError: '>' not supported between instances of 'int' and 'str'
```

##### 3 返回列表元素最小值 min(list)

使用方法和`max()`相同：

- 当列表中元素全部为字符串时，根据每个字符串中第一个字符的`ASCll` 或 `Unicode`对应的数值来比较，如果第一个字符的`ASCll` 或 `Unicode` 对应数值相同，则比较第二个字符，一次类推。

```python
list1 = ['maria', 'jack', '张三', '李四']
print(min(list1))
# jack
```

- 当列表中元素全部为数值时，直接根据数值大小来比较。

```python
list1 = [1, 2, 6, 5, 8, 9, 10, 15]
print(max(list1))
# 1
```

- 当列表中元素不全是字符串或不全是数值时，无法比较。

```python
list1 = ['maria', 'jack', '张三', '李四', 2, 5]
print(min(list1))
# TypeError: '>' not supported between instances of 'int' and 'str'
```

##### 4 将元组转换成列表 list()

> **list()** 方法用于将元组或字符串转换为列表。

**注：**元组与列表是非常类似的，区别在于元组的元素值不能修改，元组是放在括号中，列表是放于方括号中。

**语法**

```python
list(seq)
# seq 为要转换成列表的元组或字符串
```

**示例**

```python
tuple1 = (1, 3, 5, 7, 9)
print(list(tuple1))
print(list('hello,world!'))
```

输出：

```python
[1, 3, 5, 7, 9]
['h', 'e', 'l', 'l', 'o', ',', 'w', 'o', 'r', 'l', 'd', '!']
```

#### 列表方法

##### 1 在列表末尾添加新元素 list.append(obj)

> **append()** 方法用于在列表末尾添加新元素。

**语法**

```python
list.append(obj)
```

- `list` 是要操作的列表对象
- `obj` 是要添加到列表末尾的对象

- 该方法无返回值，但是修改原列表

**示例**

```python
list1 = [1, 2, 'ZhangSan']
list1.append('LiSi')
print(list1)
list1.append([5, 6, 7])
print(list1)
```

输出：

```python
[1, 2, 'ZhangSan', 'LiSi']
[1, 2, 'ZhangSan', 'LiSi', [5, 6, 7]]
```

<span style="color:red;font-weight:700;font-size:20px">注意</span>

```python
list1 = [1, 2]
list2 =[3]
list1.append(list2)
print(list1)
list2[0] = 5
print(list1, list2)
```

以上代码输出：

```python
[1, 2, [3]]
[1, 2, [5]] [5]
```

由此可见，append() 是`浅拷贝` 。

如果不希望这样，可以使用`copy.deepcopy` :需要导入`copy` 模块

```python
import copy

list1 = [1, 2]
list2 =[3]
list1.append(copy.deepcopy(list2))
print(list1)
list2[0] = 5
print(list1, list2)
```

以上代码输出：

```python
[1, 2, [3]]
[1, 2, [3]] [5]
```

##### 2 统计某个元素在列表中出现次数 list.count(obj)

> **count()** 方法用于统计某个元素在列表中出现的次数。

**语法**

```python
list.count(obj)
```

- `obj` 是需要统计的元素

**示例**

```python
list1 = [1, 1, 1, '1', 1, '1', 2, 3]
print(f"列表中有数字1的个数为：{list1.count(1)}")
print(f"列表中有字符串1的个数为：{list1.count('1')}")
```

以上代码输出：

```markdown
列表中有数字1的个数为：4
列表中有字符串1的个数为：2
```

##### 3 list.extend(seq)

> **extend()** 函数用于在列表末尾一次性追加另一个序列中的多个值（用新列表扩展原来的列表）。

**语法**

```python
list.extend(seq)
```

- `seq` --元素列表，可以是列表、元组、集合、字典，若为字典,则仅会将键(key)作为元素依次添加至原列表的末尾。

**示例**

```python
list1 = [1, 2, 3]
num_tuple = (4, 5, 6)
num_set = {7, 8, 9}
num_list = [10, 11]
num_dict = {
    12: 'twelve'
}
list1.extend(num_tuple)
print("添加元组元素后：", list1)
list1.extend(num_set)
print("添加集合元素后：", list1)
list1.extend(num_list)
print("添加列表元素后：", list1)
list1.extend(num_dict)
print("添加字典元素后：", list1)
```

以上代码输出：

```txt
添加元组元素后： [1, 2, 3, 4, 5, 6]
添加集合元素后： [1, 2, 3, 4, 5, 6, 8, 9, 7]
添加列表元素后： [1, 2, 3, 4, 5, 6, 8, 9, 7, 10, 11]
添加字典元素后： [1, 2, 3, 4, 5, 6, 8, 9, 7, 10, 11, 12]
```

##### 4 list.index()

> **index()** 函数用于从列表中找出某个值第一个匹配项的索引位置。

**语法**

```python
list.index(x[, start, end])
```

- `x` 要查找的元素
- `start` --可选，默认值为`0`,查找的起始位置
- `end` --可选，默认值为`length-1`查找的结束位置

<span style="color:red;font-weight:700;font-size:20px;">注意：查找范围包括起始索引，不包括结束索引。</span>

**示例**

```python
list1 = ['aa', 'bb', 'cc', 'aa', 'aa', 'cc', 'bb', 'dd', 'ee']
print("第一个aa的索引为：", list1.index('aa'))
print("从索引3开始的第一个aa的索引为：", list1.index('aa', 3))
print("从索引2到索引7的第一个bb的索引为：", list1.index('bb', 2, 7))
```

以上代码输出：

```python
第一个aa的索引为： 0
从索引3开始的第一个aa的索引为： 3
从索引2到索引7的第一个bb的索引为： 6
```

##### 5 在列表指定位置插入元素 list.insert()

> **insert()** 函数用于将指定对象插入列表的指定位置。

**语法**

```python
list.insert(index, obj)
```

- `index ` \-- 对象obj需要插入的索引位置。
- `obj` -- 要插入列表中的对象。

**示例**

```python
list1 = [1, 3, 4, 5]
list1.insert(1, 2)
print(list1)
```

以上代码输出：

```python
[1, 2, 3, 4, 5]
```

##### 6 移除列表中一个元素 list.pop()

> **pop()** 函数用于移除列表中的一个元素（默认最后一个元素），并且返回该元素的值。

**语法**

```python
list.pop([index])
```

- `index` --可选参数，默认为 `-1` ，不能超出列表索引范围

- 返回值为移除的元素

**示例**

```python
list1 = ['BeiJing', 'ShangHai', 'GuangZhou', 'ZhengZhou', 'HangZhou']
list_pop = list1.pop()
print(f"移除元素{list_pop}后的列表为：{list1}" )
list_pop = list1.pop(1)
print(f"移除元素{list_pop}后的列表为：{list1}" )
```

以上代码输出：

```python
移除元素HangZhou后的列表为：['BeiJing', 'ShangHai', 'GuangZhou', 'ZhengZhou']
移除元素ShangHai后的列表为：['BeiJing', 'GuangZhou', 'ZhengZhou']
```

##### 7 移除列表中某个值的第一个匹配项 list.remove()

> **remove()** 函数用于移除列表中某个值的第一个匹配项。

**语法**

```python
list.remove(obj)
```

- `obj` -- 列表中要移除的对象。

- 该方法无返回值

**示例**

```python
list1 = [1, 2, 1, 3, 6, 5, 2]
list1.remove(1)
print(list1)
```

以上代码输出：

```python
[2, 1, 3, 6, 5, 2]
```

##### 8 反向列表中的元素 list.reverse()

> **reverse()** 函数用于反向列表中元素。

**语法**

```python
list.reverse()
```

- 无参数，无返回值

**示例**

```python
list1 = [1, 2, 3, 45, 6]
list1.reverse()
print(list1)
```

以上代码输出：

```python
[6, 45, 3, 2, 1]
```

##### 9 列表排序 list.sort()

> **sort()** 函数用于对原列表进行排序，如果指定参数，则使用比较函数指定的比较函数。

**语法**

```python
list.sort(key=None, reverse=False)
```

- `key` -- 主要是用来进行比较的元素，只有一个参数，具体的函数的参数就是取自于可迭代对象中，指定可迭代对象中的一个元素来进行排序。
- `reverse` -- 排序规则，**reverse = True** 降序， **reverse = False** 升序（默认）。

**示例**

```python
list1 = [1, 2, 3, 45, 6, 10, 88]
list1.sort()
print(list1)
```

以上代码输出结果：

```python
[1, 2, 3, 6, 10, 45, 88]
```

降序排列：

```python
list1 = [1, 2, 3, 45, 6, 10, 88]
list1.sort(reverse=True)
print(list1)
```

以上代码输出：

```python
[88, 45, 10, 6, 3, 2, 1]
```

对字符串进行排序：

```python
list1 = ['BeinJing', 'ShangHai', 'GuangZhou', 'ZhengZhou']
list1.sort()
print("升序排列的结果为：", list1)
list1.sort(reverse=True)
print("降序排列的结果为：", list1)
```

字符串排序依据字符串首个字符的`Ascll` 或 `Unicode` 数值，首个字符相同则比较第二个字符。

以上代码输出：

```python
升序排列的结果为： ['BeinJing', 'GuangZhou', 'ShangHai', 'ZhengZhou']
降序排列的结果为： ['ZhengZhou', 'ShangHai', 'GuangZhou', 'BeinJing']
```

通过指定列表中的元素排序来输出列表：

```python
list1 = ['BeinJing', 'ShangHai', 'GuangZhou', 'ZhengZhou']
list1.sort(key=lambda k: k[1])
print("根据字符串的第二个字符排列的结果为：", list1)
```

以上代码输出：

```python
根据字符串的第二个字符排列的结果为： ['BeinJing', 'ShangHai', 'ZhengZhou', 'GuangZhou']
```

等价于下面的代码：

```python
list1 = ['BeinJing', 'ShangHai', 'GuangZhou', 'ZhengZhou']

def secondchar(e):
    return e[1]

list1.sort(key=secondchar)
print("根据字符串的第二个字符排列的结果为：", list1)
```

##### 10 清空列表 list.clear()

> **clear()** 函数用于清空列表，类似于 **del a[:]**。

**语法**

```python
list.clear()
```

**示例**

```python
list1 = [1, 2, 3, 4, 5]
list1.clear()
print(list1)
```

以上代码输出：

```python
[]
```

##### 11 复制列表 list.copy()

> **copy()** 函数用于复制列表，类似于 **a[:]**。

**语法**

```python
list.copy()
```

**示例**

```python
list1 = [1, 2, 3, 4, 5, 6]
list2 = list1.copy()
print(list2)
```

以上代码输出：

```python
[1, 2, 3, 4, 5, 6]
```

实验：

```python
list1 = [1, 2, 3, 4, 5, 6]
list2 = list1.copy()
list1[1] = 10
print(list1, list2)
# [1, 10, 3, 4, 5, 6] [1, 2, 3, 4, 5, 6]
```

再看下面：

```python
list1 = [[8, 9], 1, 2, 3, 4, 5, 6]
list2 = list1.copy()
list1[0][1] = 10
print(list1, list2)
# [[8, 10], 1, 2, 3, 4, 5, 6] [[8, 10], 1, 2, 3, 4, 5, 6]
```

说明copy()方法是浅拷贝。使用时需要注意。