### 创建数据库连接

```python
import pymysql

conn = pymysql.connect(host="localhost", port=3306, user="root", password="123456", database="database_name", charset="utf8")
```

创建数据库连接时 ，接收以下参数：

**参数：**

- **host**-数据库服务器的主机地址。
- **user**-登录数据库的用户名。
- **password**-登录数据库的密码。
- **database**-使用数据库，None(不指定值)表示不使用特定的数据库。
- **port**-MySQL的端口，默认为3306。
- **charset**-指定字符编码。
- **bind_address**-当客户端有多个网络接口时，请指定连接主机的接口。参数可以是主机名或IP地址。
- **read_timeout**-从连接读取数据的超时时间，以秒为单位(默认值为None，不设置超时时间)。
- **write_timeout**-写入连接的超时时间，以秒为单位(默认值为None，不设置超时时间)。
- **cursorclass**-自定义游标的类型。

连接对象有以下方法：

- **.close()**

    - 立刻关闭当前的连接。
    - 从这一点开始，连接将不可用;如果试图对连接进行任何操作，将引发Error(或子类)异常。如果使用该连接创建的游标，同样会引发异常。
    - 请注意，在不首先提交更改的情况下关闭连接将导致执行隐式回滚。

- **.commit()**

    - 将更改提交到数据库。

- **.cursor()**

    - 使用连接返回一个新的游标对象。

    - 参数：指定获取的数据如何存放，默认值为None，使用Cursor类型。

        ```python
        import pymysql
        conn = pymysql.connect(host="localhost", port=3306, user="root",
                               password="123456", database="db", charsrt="utf8")
        conn.cursor(cursor=None)
        # pymysql.cursors.DictCursor
        # pymysql.cursors.Cursor
        # pymysql.cursors.SSCursor
        # pymysql.cursors.SSDictCursor
        # pymysql.cursors.DictCursorMixin
        ```

- **.open()**

    - 如果连接是打开的，返回True。

- **.ping(reconnect=True)**

    - 检查服务器是否处于活动状态。
    - 如果连接是关闭状态，reconnect=True时，重启连接，reconnect=False时，引发Error异常。

- **.rollback()**

    - 回滚当前事务

- **.select_db(database_name)**

    - 设置当前数据库

- **.show_warnings()**

    - 发送“SHOW WARNINGS”SQL命令

创建游标：

```python
cursor = conn.cursor()
```

游标对象的属性：

- **.description**
    - 只读属性，返回字段的详细信息包括：
        - `name`：字段名
        - `type_code`：字段类型码
        - `display_size`：显示尺寸
        - `internal_size`：内部大小
        - `precision`：精确度
        - `scale`：规模
        - `null_ok`：是否允许为空
- **.rownumber**
    - 返回游标所在行
- **.rowcount**
    - 只读属性，并返回执行 `execute()` 方法后影响的行数
- **.arraysize**
    - 此读取/写入属性指定使用 `.fetchmany()` 一次提取的行数。它默认为1
- **.connection**
    - 此只读属性返回创建光标的Connection对象的引用。
- **.\_\_iter\_\_**
    - 将游标转换为可迭代对象
- **.lastrowid**
    - 获取新创建数据的自增ID

游标对象方法：

- **.close()**

    - 关闭游标。也可以直接关闭连接，从而关闭游标

- **.execute(operation [, parameters])**

    - 执行数据库命令，`.execute('operation', (parameters))` 使用类似C风格的字符串格式化方法，可以防止SQL注入攻击

- **.executemany(query, args)**

    - 批量操作，这里的参数可以是可迭代对象组成的序列

    - ```mysql
        cursor.executemany('insert into student values (%s, %s, %s)', [
            														('张三', '123456', '男'), 
            														('李四', '123456', '男'),
                                                                  		 ('小明', '123456', '男')])
        ```

- **.fetchone()**
    - 获取查询结果集的下一行，返回单个序列，或在没有更多数据可用时返回`None`
- **.fetchmany([ size = cursor.arraysize ])**
    - 获取查询结果的下一组行，返回一系列序列（例如元组列表）。没有更多行可用时返回空序列。
- **.fetchall()**
    - 获取查询结果的所有（剩余）行，并将它们作为一系列序列（例如元组列表）返回。请注意，游标的arraysize属性可能会影响此操作的性能。
- **.mogrify(query,args=None)**
    - 参数化查询，防止SQL注入攻击，通过调用 `execute()` 方法返回发送到数据库的确切字符串。

执行数据库操作：

```python
sql = "select * from table_name"
cursor.execute(sql)
result = cursor.fetchall()
```

