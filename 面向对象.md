### 面向对象技术简介

**类(class)：**用来描述具有相同的属性和方法的对象的集合。它定义了该集合中每个对象所共有的属性和方法。对象是类的实例。

**实例化：**创建一个类的实例，类的具体对象。

**类方法：**类中定义的函数。

> 类是抽象的模板，比如Student类，而实例是根据类创建出来的一个个具体的“对象”，每个对象都拥有相同的方法，但各自的数据可能不同。

### 创建类

在Python中，定义类是通过`class`关键字：

```python
class Student([extendClass]):
    pass
```

`class`后面紧接着是类名，类名通常时大写字母开头，如果该类是从某个其他类继承下来的，类名后面需要跟上继承自哪个类。

#### 创建一个实例

根据`Student`类，创建一个`Student`的实例:

```python
stu1 = Student()
```

可以看出，`stu1`就是`Student`类的一个实例化对象：

![image-20211011121609008](https://i.loli.net/2021/10/11/37Pwgxz4vU1M5dq.png)

可以自由地给一个实例变量绑定属性，比如，给实例`stu1`绑定一个`sname`属性：

![image-20211011121851836](https://i.loli.net/2021/10/11/sLiuNnUMK5Qh8VS.png)

由于类可以起到模板的作用，因此，可以在创建实例的时候，把一些我们认为必须绑定的属性强制填写进去。通过定义一个特殊的`__init__`方法，在创建实例的时候，就把`sname`，`score`等属性绑上去：

```python
class Student:
    def __init__(self, sname, sorce):
        self.sname = sname
        self.sorce = sorce
```

:notebook:**注意：**特殊方法`__init__`前后分别有两个下划线。

`__init__`方法的第一个参数永远是`self` ，表示创建的实例本身。因此，在方法内部就可以把各种属性绑定到`self`，因为`self`就指向创建的实例本身。

有了`__init`方法，在创建实例的时候就需要传入和`__init__`方法相匹配的参数。其中`self`参数不需要传递，Python会自己把实例的对象传给`self`。

![image-20211011133308283](https://i.loli.net/2021/10/11/CPbnAQ4iVSUqdvy.png)

和普通的函数相比，在类中定义的函数只有一点不同，就是第一个参数永远是实例变量`self`，并且，调用时，不用传递该参数。除此之外，类的方法和普通函数没有什么区别，所以，你仍然可以用默认参数、可变参数、关键字参数和命名关键字参数。

#### 数据封装

面向对象编程的一个重要特点就是数据封装。在上面的`Student`类中，每个实例就拥有各自的`name`和`score`这些数据。我们可以通过函数来访问这些数据，比如打印一个学生的成绩。

在类的内部定义一个访问数据的函数，这些函数称为：类的方法。

```python
class Student:
    def __init__(self, sname, score):
        self.sname = sname
        self.score = score

    def print_score(self):
        print(f"{self.sname}的成绩为：{self.score}")
```

要定义一个类的方法，除了第一个参数是`self`外，其他和普通函数一样。

那么如何调用一个类的方法呢？只需要在实例变量上直接调用，传入除`self` 之外的其他参数：

```python
stu2 = Student("李四", 99)
stu2.print_score()
```

![image-20211011134649819](https://i.loli.net/2021/10/11/8eQ1LlNvsk6pwdD.png)

这样一来，我们从外部看`Student`类，就只需要知道，创建实例需要给出`name`和`score`，而如何打印，都是在`Student`类的内部定义的，这些数据和逻辑被“封装”起来了，调用很容易，但却不用知道内部实现的细节。

封装的另一个好处是可以给`Student`类增加新的方法，比如`get_grade`：

```python
class Student(object):
    ...

    def get_grade(self):
        if self.score >= 90:
            return 'A'
        elif self.score >= 60:
            return 'B'
        else:
            return 'C'
```

##### 小结

类是创建实例的模板，而实例则是一个一个具体的对象，各个实例拥有的数据都互相独立，互不影响；

方法就是与实例绑定的函数，和普通函数不同，方法可以直接访问实例的数据；

通过在实例上调用方法，我们就直接操作了对象内部的数据，但无需知道方法内部的实现细节。

### 访问限制

在Class内部，可以有属性和方法，而外部代码可以通过直接调用实例变量的方法来操作数据，这样，就隐藏了内部的复杂逻辑。

但是，从前面Student类的定义来看，外部代码还是可以自由地修改一个实例的`name`、`score`属性：

```python
stu1 = Student("张三", 80)
stu1.print_score()
stu1.score = 90
stu1.print_score()
```

![image-20211011135802236](https://i.loli.net/2021/10/11/rSJyOLkAlW9a5TG.png)

如果要让内部属性不被外部访问，可以把属性的名称前加上两个下划线`__`，在Python中，实例的变量名如果以`__`开头，就变成了一个私有变量（private），只有内部可以访问，外部不能访问，所以，我们把Student类改一改：

```python
class Student:
    def __init__(self, sname, score):
        self.__sname = sname
        self.__score = score

    def print_score(self):
        print(f"{self.__sname}的成绩为：{self.__score}")
```

改完后，对于外部代码来说，没什么变动，但是已经无法从外部访问`实例变量.__name`和`实例变量.__score`了：

![image-20211011140145743](https://i.loli.net/2021/10/11/SxhPrvwpFjW31zV.png)

这样就确保了外部代码不能随意修改对象内部的状态，这样通过访问限制的保护，代码更加健壮。

但是如果外部代码要获取name和score怎么办？可以给Student类增加`get_name`和`get_score`这样的方法：

```python
class Student(object):
    ...
    
    def get_name(self):
        return self.__name
    
    def get_score(self):
        return self.__score
```

![image-20211011140931997](https://i.loli.net/2021/10/11/oZ13tXenJdzLSsT.png)

如果又要允许外部代码修改score怎么办？可以再给Student类增加`set_score`方法：

```python
class Student(object):
    ...

    def set_score(self, score):
        self.__score = score
```

![image-20211011141134641](https://i.loli.net/2021/10/11/n3JBsGWRvKAM1dO.png)

这样做还有一个好处，就是可以对参数进行检查，避免传入无效的参数：

```python
    def set_score(self, score):
        if score < 0 or score > 100:
            print("参数无效")
        else:
            self.__score = score
```

![image-20211011141453014](https://i.loli.net/2021/10/11/xmHnigERFsZUWPX.png)

<span style="background-color:yellow;font-weight:700;">注意下面这种错误写法：</span>

![image-20211011142105317](https://i.loli.net/2021/10/11/BOYhVnc9XGvHDqJ.png)

表面上看来，“成功”地设置了`__sname`变量，但实际上这个`__sname`和class内部的`__sname`并不是同一个变量，上面的代码只是给实例`stu1`新增了一个属性，命名为`__sname`，而内部的私有变量`__sname`已经被Python解释器自动更改成了其他的名字。

### 继承和多态

在面向对象程序设计中，当我们定义一个class的时候，可以从某个现有的class继承，新的class称为子类(Subclass)或派生类，而被继承的class称为基类(Base class)、父类或超类(Super class)。

比如我们已经编写了一个名为`Animal`的class，有一个`run()`方法：

```python
class Animal:
    def run(self):
        print('Animal is runing...')
```

当我们需要编写`Dog`和`Cat`类时，就可以直接从`Animal`类继承：

```python
class Dog(Animal):
    pass


class Cat(Animal):
    pass
```

对于`Dog`和`Cat`来说，`Animal`就是它们的父类。对于`Animal`来说，`Dog`和`Cat`就是它的子类。

继承有什么好处？最大的好处是子类获得了父类的全部功能。由于`Animial`实现了`run()`方法，因此，`Dog`和`Cat`作为它的子类，什么事也没干，就自动拥有了`run()`方法：

```python
dog = Dog()
dog.run()

cat = Cat()
cat.run()
```

![image-20211011152732666](https://i.loli.net/2021/10/11/RWeV5BIrbv6QdiF.png)

当然，也可以对子类增加一些方法，比如Dog类：

```python
calss Dog:
    def eat(self):
        print('the dog is eating')
```

![image-20211011153152135](https://i.loli.net/2021/10/11/Hx4WEV6GUv3AiZ2.png)

继承的第二个好处需要我们对代码做一点改进。你看到了，无论是`Dog`还是`Cat`，它们`run()`的时候，显示的都是`Animal is running...`，符合逻辑的做法是分别显示`Dog is running...`和`Cat is running...`，因此，对`Dog`和`Cat`类改进如下：

```python
class Dog(Animal):

    def run(self):
        print('Dog is running...')

class Cat(Animal):

    def run(self):
        print('Cat is running...')
```

![image-20211011153427682](https://i.loli.net/2021/10/11/yGLbtu9US5DHCVA.png)

当子类和父类都存在相同的`run()`方法时，我们说，子类的`run()`覆盖了父类的`run()`，在代码运行的时候，总是会调用子类的`run()`。这样，我们就获得了继承的另一个好处：多态。

要理解什么是多态，我们首先要对数据类型再作一点说明。当我们定义一个class的时候，我们实际上就定义了一种数据类型。我们定义的数据类型和Python自带的数据类型，比如str、list、dict没什么两样：

```python
a = list() # a是list类型
b = Animal() # b是Animal类型
c = Dog() # c是Dog类型
```

判断一个变量是否是某个类型可以用`isinstance()`判断：

![image-20211011153826038](https://i.loli.net/2021/10/11/bBhZf7PF2XcsQVK.png)

由于`Dog`是`Animal`的子类，所以创建一个`Dog`的实例`c`时，我们认为`c`的数据类型是`Dog`，同时`c`也是`Animal`。

所以：

![image-20211011154359129](https://i.loli.net/2021/10/11/KR8AFEOHfkMrJeq.png)

所以，在继承关系中，如果一个实例的数据类型是某个子类，那它的数据类型也可以被看做是父类。但是，反过来就不行：

![image-20211011154530634](https://i.loli.net/2021/10/11/vW1FpsPBmltVK8e.png)

#### 子类继承父类构造函数

如果重写了**\__init__** 时，实例化子类，就不会调用父类已经定义的 **\__init__**，语法格式如下：

```python
class Father(object):
    def __init__(self, name):
        self.name = name
        print("name: %s" % self.name)

    def getName(self):
        return 'Father ' + self.name


class Son(Father):
    def __init__(self, name):
        print("hi")
        self.name = name

    def getName(self):
        return 'Son ' + self.name

son = Son('Python')
print(son.getName())
```

![image-20211011164805662](https://i.loli.net/2021/10/11/2shPCgcY1qKvtoR.png)

如果在子类中需要父类的构造方法就需要显式地调用父类的构造方法，或者不重写父类的构造方法。

1.子类不重写 **\__init__**，实例化子类时，会自动调用父类定义的 **\__init__**。

```python
class Father(object):
    def __init__(self, name):
        self.name=name
        print ( "name: %s" % self.name )
    def getName(self):
        return 'Father ' + self.name
 
class Son(Father):
    def getName(self):
        return 'Son '+self.name
 
son=Son('runoob')
print ( son.getName() )
```

![image-20211011164905417](https://i.loli.net/2021/10/11/OI4hFGMmlvESkRg.png)

如果重写了 **\__init__**  时，要继承父类的构造方法，需要使用 **super** 关键字：

```python
# 第一种方法
super(子类, self).__init__(参数1, 参数2...)
# 第二种方法
父类.__init__(self, 参数1, 参数2...)
```

```python
class Father(object):
    def __init__(self, name):
        self.name = name
        print("name: %s" % self.name)

    def getName(self):
        return 'Father ' + self.name


class Son(Father):
    def __init__(self, name):
        super(Son, self).__init__(name)
        # Father.__init__(self, name)
        print("hi")
        self.name = name

    def getName(self):
        return 'Son ' + self.name

son = Son('Python')
print(son.getName())
```

![image-20211011164645469](https://i.loli.net/2021/10/11/aqo9AIyCUcMXk58.png)

