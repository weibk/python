### 安装浏览器驱动

浏览器驱动 是和 浏览器对应的。 不同的浏览器 需要选择不同的浏览器驱动。

目前主流的浏览器中，谷歌 Chrome 浏览器对 Selenium自动化的支持更加成熟一些。强烈推荐大家使用 Chrome浏览器。

[点击下载Chrome浏览器](https://www.google.cn/chrome/)

确保Chrome浏览器安装好以后，打开下面的连接，访问Chrome 浏览器的驱动下载页面

[Chrome 浏览器驱动下载地址](https://chromedriver.storage.googleapis.com/index.html)

下载和自己浏览器版本号最接近的驱动：

<img src="https://i.loli.net/2021/08/16/mn8VMba2jHWKYCv.png" alt="image-20210816140842551" style="zoom:67%;" />

查看自己浏览器的版本号：

<img src="https://i.loli.net/2021/08/16/A8sTaCkl1XFvh3z.png" alt="image-20210816144013278" style="zoom:67%;" />



<img src="https://i.loli.net/2021/08/16/7LoHnsMGmhazJNP.png" alt="image-20210816144138690" style="zoom:67%;" />

打开目录，里面有3个zip包，分别对应Linux、Mac、Windows平台。

![image-20210816141111166](https://i.loli.net/2021/08/16/q1GXbNyjBVQC4nU.png)

本文将以`Windows`系统为例。

点击`chromedriver_win32.zip`进行下载。下载后解压出来。解压后所在位置的路径最好不要有中文或者空格。

### 将浏览器驱动所在目录添加环境变量

为了以后的使用方便，可以把驱动所在的目录添加到环境变量里。

![image-20210816144703883](https://i.loli.net/2021/08/16/J6omKDaTwuiyCsZ.png)

双击Path变量：

<img src="https://i.loli.net/2021/08/16/8p7l6Kfm4nEFuL9.png" alt="image-20210816143220257" style="zoom:67%;" />

如果电脑上同时有多个用户，并且希望每个用户都可以使用浏览器驱动，也可以直接在系统变量中的Path变量中添加。

点击新建：

<img src="https://i.loli.net/2021/08/16/xL8AGiDNIwlgrEm.png" alt="image-20210816143446785" style="zoom:67%;" />

点击浏览，找到刚才解压的文件夹：

<img src="https://i.loli.net/2021/08/16/ixgbT6L82cmYjuH.png" alt="image-20210816143612511" style="zoom:67%;" />

一路点击确定。

现在已经完成了驱动的环境变量的添加。

### 将浏览器添加至环境变量

按照驱动添加环境变量的方法，将Chrome浏览器的 安装目录添加到环境变量中，即chrome.exe文件所在的目录。

### 简单体验

新建一个项目：

<img src="https://i.loli.net/2021/08/16/Fp3QqtJAoG48l7Z.png" alt="image-20210816155513898" style="zoom:80%;" />

在项目中添加`selenium`模块：

<img src="https://i.loli.net/2021/08/16/CcI4FGdzJRvWY3X.png" alt="image-20210816155726106" style="zoom:67%;" />

也可以在电脑中全局安装`selenium` 模块：

```bash
pip install selenium
```

新建一个Python文件：

```python
from selenium import webdriver
#导入webriver

wd = webdriver.Chrome()
#创建 WebDriver 对象，指明使用chrome浏览器驱动
wd.get('https://www.baidu.com')
#打开百度页面
kw = wd.find_element_by_id('kw')
#获取到搜索框
kw.send_keys('Python\n')
#输入Python并回车
```

执行代码：

<img src="https://i.loli.net/2021/08/16/r1CsLTvXWRyShK8.png" alt="image-20210816160438454" style="zoom:67%;" />

