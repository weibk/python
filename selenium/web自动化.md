### 元素定位

#### 查找单个元素：

```python
driver.find_element_by_id()
driver.find_element_by_name()
driver.find_element_by_xpath()
driver.find_element_by_link_text()
driver.find_element_by_partial_link_text()
driver.find_element_by_tag_name()
driver.find_element_by_class_name()
driver.find_element_by_css_selector()
```

> 如果有多个标签名相同的元素或类名相同的元素，则只返回第一个出现的。

#### 查找多个元素：

```python
driver.find_elements_by_name()
driver.find_elements_by_xpath()
driver.find_elements_by_link_text()
driver.find_elements_by_partial_link_text()
driver.find_elements_by_tag_name()
driver.find_elements_by_class_name()
driver.find_elements_by_css_selector()
```

> 以上方法返回一个 **list** 。

除了上面这些公有的方法，我们还有2个私有的方法来帮助页页面对象的定位。这两个方法就是`find_element`和`find_elements`:

```python
from selenium.webdriver.common.by import By

driver.find_element(By.XPATH,'//button[text()="Some Text"]')
driver.find_elements(By.XPATH,'//button')
```

`By`类的可用属性如下：

```python
ID = "id"
XPATH = "xpath"
LINK_TEXT = "link text"
PARTIAL_LINK_TEXT = "partial link text"
NAME = "name"
TAG_NAME = "tag_name"
CLASS_NAME = "class name"
CSS_SELECTOR = "css selector"
```

### 控制浏览器操作

```python
# 控制浏览器窗口大小
driver.set_window_size(480, 800)

# 浏览器后退，前进
# 后退 
driver.back()

# 前进 
driver.forward()

# 刷新
driver.refresh() # 刷新
```

### 元素操作

```python
driver.find_element_by_id("kw").clear() # 清楚文本 
driver.find_element_by_id("kw").send_keys("selenium") # 模拟按键输入 
driver.find_element_by_id("su").click() # 单击元素
```

模拟表单提交：

```python
search_text = driver.find_element_by_id('kw') # 获取到搜索框
search_text.send_keys('selenium') # 输入内容
search_text.submit() # 模拟提交动作
```

其它：

- **size**：返回元素尺寸。
- **text**：获取元素文本。
- **get_attribute(name)**：获取属性值。
- **is_displayed()**：是否用户可见。

#### 鼠标操作

在 WebDriver 中， 将这些关于鼠标操作的方法封装在 ActionChains 类提供。

ActionChains 类提供了鼠标操作的常用方法：

- **perform()**： 执行所有 ActionChains 中存储的行为；
- **context_click()**： 右击；
- **double_click()**： 双击；
- **drag_and_drop()**： 拖动；
- **move_to_element()**： 鼠标悬停。

```python
from selenium import webdriver
from selenium.webdriver.commom.action_chains import ActionChains
# 创建 WebDriver 对象，指明使用chrome浏览器驱动
wd = webdriver.Chrome()
# 打开百度页面
wd.get('https://www.baidu.com')
# 定位到要悬停的元素
above = wd.find_element_by_link_text("更多")
# 对定位到的元素执行鼠标悬停操作
ActionChains(wd).move_to_element(above).perform()
```

![image-20210817150638901](https://i.loli.net/2021/08/17/JXQSh8j9zRsudnO.png)

#### 键盘操作

常用的键盘操作：

- send_keys(Keys.BACK_SPACE) **删除键**（BackSpace）
- send_keys(Keys.SPACE) **空格键** (Space)
- send_keys(Keys.TAB) **制表键** (Tab)
- send_keys(Keys.ESCAPE) **回退键**（Esc）
- send_keys(Keys.ENTER) **回车键**（Enter）
- send_keys(Keys.CONTROL,'a') **全选**（Ctrl+A）
- send_keys(Keys.CONTROL,'c') **复制**（Ctrl+C）
- send_keys(Keys.CONTROL,'x') **剪切**（Ctrl+X）
- send_keys(Keys.CONTROL,'v') **粘贴**（Ctrl+V）
- send_keys(Keys.F1) **键盘 F1**
- ……
- send_keys(Keys.F12) **键盘 F12**

```python
from selenium import webdriver # 导入webriver
from selenium.webdriver.common.keys import Keys

wd = webdriver.Chrome()
# 创建 WebDriver 对象，指明使用chrome浏览器驱动
wd.get('https://www.baidu.com')
# 打开百度页面
kw = wd.find_element_by_id('kw')
# 获取到搜索框
kw.send_keys('Python')
# 输入Python
kw.send_keys(Keys.ENTER)
# 模拟回车动作
```

#### 获取断言信息

```python
title = driver.title # 当前页面title
now_url = driver.current_url # 当前页面URL
```

#### 等待当前页面加载完成

**显式等待**

```python
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

driver = webdriver.Firefox()
driver.get("http://www.baidu.com")

element = WebDriverWait(driver, 5, 0.5).until(
                      EC.presence_of_element_located((By.ID, "kw"))
                      )
# 直到要获取的元素加载出来以后，执行下一步
element.send_keys('selenium')
driver.quit()
```

WebDriverWait类是由WebDirver 提供的等待方法。在设置时间内，默认每隔一段时间检测一次当前页面元素是否存在，如果超过设置时间检测不到则抛出异常。具体格式如下：

```python
WebDriverWait(driver, timeout, poll_frequency=0.5, ignored_exceptions=None)
```

- **driver** ：浏览器驱动。
- **timeout** ：最长超时时间，默认以秒为单位。
- **poll_frequency **：检测的间隔（步长）时间，默认为0.5S。
- **ignored_exceptions** ：超时后的异常信息，默认情况下抛NoSuchElementException异常。
- WebDriverWait()一般由until()或until_not()方法配合使用，下面是until()和until_not()方法的说明。
- **until(method, message=‘’)** 调用该方法提供的驱动程序作为一个参数，直到返回值为True。
- **until_not(method, message=‘’)** 调用该方法提供的驱动程序作为一个参数，直到返回值为False。

在本例中，通过as关键字将expected_conditions 重命名为EC，并调用presence_of_element_located()方法判断元素是否存在。

**隐式等待**

如果某些元素不是立即可用的，隐式等待是告诉WebDriver去等待一定的时间后去查找元素。 默认等待时间是0秒，一旦设置该值，隐式等待是设置该WebDriver的实例的生命周期。

```python
from selenium import webdriver
driver = webdriver.Firefox()    
driver.implicitly_wait(10) # seconds    
driver.get("http://somedomain/url_that_delays_loading")    
myDynamicElement = driver.find_element_by_id("myDynamicElement") 
```

